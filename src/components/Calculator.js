import React from "react";
import { useState } from "react";
import { Container, Form, Button } from "react-bootstrap";

export default function Display() {

    const [num1, setNum1] = useState('');
    const [num2, setNum2] = useState('');
    const [result, setResult] = useState(0);

    function Add() {
        let result = num1 + num2;
        return (
            setResult(result)
            )
    }

    function Subtract() {
        let result = num1 - num2;
        return (
            setResult(result)
            )
    }

    function Multiply() {
        let result = num1 * num2;
        return (
            setResult(result)
            )
    }

    function Divide() {
        let result = num1 / num2;
        return (
            setResult(result)
            )
    }

    function Reset() {
        setNum1('');
        setNum2('');
        setResult(0);
    }

    return (
        <Container className="container m-5 text-center">
            <h1>Calculator</h1>
            <h2>{result}</h2>
            
            <Form className="form">
                <Form.Group className="mb-3" controlId="formDisplay">
                    <Form.Control type="text" placeholder="Number 1" value={num1} onChange={(e) => setNum1(parseInt(e.target.value))}/>
                    <Form.Control type="text" placeholder="Number 2" value={num2} onChange={(e) => setNum2(parseInt(e.target.value))}/>
                </Form.Group>
                
            </Form>

            <Button variant="primary" onClick={() => Add()}>Add</Button>{' '}
            <Button variant="primary" onClick={() => Subtract()}>Subtract</Button>{' '}
            <Button variant="primary" onClick={() => Multiply()}>Multiply</Button>{' '}
            <Button variant="primary" onClick={() => Divide()}>Divide</Button>{' '}
            <Button variant="secondary" onClick={() => Reset()}>Reset</Button>{' '}

        </Container>
    );
};
